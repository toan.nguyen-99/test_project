- Câu try lệnh cho phép bạn kiểm tra một khối mã để tìm lỗi.
- Câu catch lệnh cho phép bạn xử lý lỗi.
- Câu throw lệnh cho phép bạn tạo các thông báo lỗi tùy chỉnh.
- Câu finally lệnh cho phép bạn thực thi mã, sau khi thử và bắt, bất kể kết quả.
- Cú pháp try...cath:
    try {
        //Block of code to try
    }
    catch(err) {
        //Block of code to handle errors
    }
- Mã được thục thi trong try{} nếu lỗi sẽ thực hiệ  các câu lệnh trong cath
- Cú pháp finally:
    try {
        //Block of code to try
    }
    catch(err) {
        //Block of code to handle errors/
    }
    finally {
        // of code to be executed regardless of the try / catch result
    }
- Bất kể try..cath đúng hay sai đều thực hiện câu lệnh trong finally
