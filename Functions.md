Hàm JavaScript là một khối mã được thiết kế để thực hiện một tác vụ cụ thể.
Một hàm JavaScript được thực thi khi "cái gì đó" gọi nó.
- Cú pháp:
    function name(parameter1, parameter2, parameter3) {
    // code to be executed
    }
- các câu lệnh trong hàm được thực thi khi:
    + Khi một sự kiện xảy ra
    + Khi nó được gọi
    + Tự động
- Để gọi hàm: [function_name]()d
- Nếu hàm có tham số, ta cần truyền tham số vào trong hàm