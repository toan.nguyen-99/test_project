1. Vòng lặp for()
- Cú pháp: 

  for ([initialization];[condition];[final-expression]){
    Block of code
  }
 - Với vòng lặp for ta sẽ khởi tạo biến đếm, kiểm tra điều kiện và tăng hoặc giảm biến được thực hiện trên cùng một dòng

 2. Vòng lặp while
- Cú pháp:

  while (condition) {
    Block of code
  }
- Với vòng lặp while khi dc khởi chạy chương trình sẽ kiểm tra điều kiện trong while, nếu đk là true thì vòng lặp tiếp tục chạy còn nếu là false thì sẽ dừng vòng lặp

3. Vòng lặp do...while
- Cú pháp:

  do {
    Block of code
  }
  while (condition);
- Với vòng lặp do..while, vòng lặp sẽ chạy câu lệnh trong do trước rồi mới kiểm tra đk trong while. Nếu đk là true thì vòng lặp tiếp tục lặp lại khối lệnh trong do còn nếu là false thì sẽ dừng vòng lặp

4. Vòng lặp forEach
- Cú pháp:

  arrayName.forEach(function(currentValue, index, array){
      function body
  })
- Với vòng lặp forEach, sẽ lặp lại từng phần tử trong mảng theo thứ tự index và thực thi function được truyền vào

5. Vòng lặp for..in
- Cú pháp: 
  for (variableName in object) {
      Block of code
  }
- For...in mục đích chủ yếu được dùng để lặp trong một object chứ không phải array . Số lượng vòng lặp sẽ tương ứng với số lượng thuộc tính của object

6. Vòng lặp for..of
- Cú pháp:
  for (variable of iterable) {
    Block of code
  }
- Hàm có thể sử dụng để duyệt phần lớn các đối tượng từ Array, String, Map, WeakMap, Set ,...


