- Biến là vùng lưu trữ dữ liệu
- Có 3 cách để khai báo biến là sử dụng var, let, const
    + Với var có thể khai báo lại các biến được xác định trong cùng 1 khối còn let thì không
    + var có thể sử dụng biến trước khi nó được khai báo còn let nếu sử dụng một biến trước khi nó được khai báo sẽ dẫn đến ReferenceError
    + Với const hkông thể khai báo lại các biến được xác định với const
    + Const Không thể gán lại các biến được xác định
    + Biến đã được xác định với const có phạm vi khối
- Nếu 1 mảng, đối tượng được khai báo bởi const thì:
    + Có thể thay đổi các phần tử của một mảng không đổi nhưng không thể gán lại mảng
    + Có thể thay đổi các thuộc tính của một đối tượng hằng số nhưng không thể gán lại đối tượng

