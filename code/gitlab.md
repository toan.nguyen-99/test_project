Git global setup
git config --global user.name "Nguyễn Dinh"
git config --global user.email "ng.toan99cntt@gmail.com"

Create a new repository
git clone https://gitlab.com/toan.nguyen-99/test_project.git
cd test_project
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/toan.nguyen-99/test_project.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/toan.nguyen-99/test_project.git
git push -u origin --all
git push -u origin --tags