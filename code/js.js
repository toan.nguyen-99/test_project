function drawTriangle(){
    for(var i=0; i<5; i++){
        for(var j=0; j<5; j++){
            if(i>=j){
                document.writeln(" * ");
            }
        }
        document.writeln("<br>");
    }
    document.writeln("<br><br>");
}
function drawSquare(){
    for(var i=0; i<5; i++){
        for(var j=0; j<5; j++){
            if(i==0 || i==4 ||  j==0 || j==4){
                document.writeln("*");
            }else{
                document.writeln("_ ");
            }
        }
        document.writeln("<br>");
    }
    document.writeln("<br><br>");
}
function drawInvertedTriangle(){
    var n=5,b=0;
    var a=n*2-1;
    while(n>0){
        for(var i=0; i<b; i++)
        document.writeln("_");
        for(var j=0; j<a; j++)
        document.writeln("*");
        a-=2;
        b++;
        n--;
        document.writeln("<br>");
    }
    document.writeln("<br><br>");
}
function drawEmptyTriangle(){
    var n=5;
    var b=n-1;
    for(var i=0; i<n-1; i++){
        for(var j=0; j<n*2-1; j++){
            if(j==b-i || j==b+i) document.writeln('*');
            else document.writeln('_');
        }
        document.writeln("<br>");
    }
    for(var i=0; i<n*2-1;i++ ){
        document.writeln('*');
    }
}
drawTriangle();
drawSquare();
drawInvertedTriangle();
drawEmptyTriangle();
